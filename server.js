const express = require('express')
const bodyParser = require('body-parser')
const studenthomeroutes = require('./src/routes/studenthome.routes')
const mealroutes = require('./src/routes/meal.routes')
const userroutes = require('./src/routes/user.routes')
const inforoutes = require('./src/routes/info.routes')
const authenticationroutes = require('./src/routes/authentication.routes')
const config = require('./src/config/logger')
const logger = config.logger

const app = express()
app.use(bodyParser.json())

const port = process.env.PORT || 3000

app.all('*', (req, res, next) => {
  const method = req.method
  logger.debug('Method:', method)
  next()
})

app.use('/api', authenticationroutes)
app.use('/api', inforoutes)
app.use('/api', studenthomeroutes)
app.use('/api', mealroutes)
app.use('/api', userroutes)

app.all('*', (req, res, next) => {
  logger.info('Error message sent: endpoint did not exist')
  res.status(404).json({
    error: 'endpoint does not exist!'
  })
})

module.exports = app.listen(port, () => console.log(`Server listening at port ${port}`))
