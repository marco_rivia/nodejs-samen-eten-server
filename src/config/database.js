const mysql = require('mysql')
const logger = require('./logger').logger
const dbconfig = require('./logger').dbconfig

const pool = mysql.createPool(dbconfig)

module.exports = pool
