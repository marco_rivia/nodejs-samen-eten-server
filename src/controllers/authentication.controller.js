const assert = require('assert')
const jwt = require('jsonwebtoken')
const pool = require('../config/database')
const config = require('../config/logger')
const logger = config.logger
const bcrypt = require('bcrypt')
const saltRounds = 10

module.exports = {
  login(req, res, next) {
    pool.getConnection((err, connection) => {
      if (err) {
        logger.error('Error getting connection from pool')
        res
          .status(500)
          .json({ error: err.toString(), datetime: new Date().toISOString() })
      }
      if (connection) {
        connection.query(
          'SELECT `ID`, `Email`, `Password`, `First_Name`, `Last_Name` FROM `user` WHERE `Email` = ?',
          [req.body.email],
          (err, rows, results) => {
            connection.release()
            if (err) {
              logger.error('Error: ', err.toString())
              res.status(500).json({
                error: err.toString(),
                datetime: new Date().toISOString()
              })
            }
            if (rows.length > 0) {
              logger.info('Result from database: ')
              logger.info('Password:', rows[0].Password)
              logger.info(rows)

              const hash = rows[0].Password

              bcrypt.compare(req.body.password, hash, function (err, response) {
                if (response === true) {
                  logger.info('passwords DID match, sending valid token')
                  const payload = {
                    id: rows[0].ID
                  }
                  const userinfo = {
                    token: jwt.sign(payload, 'secret', { expiresIn: '2h' }),
                    username: rows[0].First_Name + ' ' + rows[0].Last_Name
                  }
                  res.status(200).json(userinfo)
                } else {
                  logger.info('User not found or password is invalid')
                  res.status(400).json({
                    error: 'User not found or password is invalid',
                    datetime: new Date().toISOString()
                  })
                }
              })
            } else {
              logger.info('User not found or password is invalid')
              res.status(400).json({
                error: 'User not found or password is invalid',
                datetime: new Date().toISOString()
              })
            }
          }
        )
      }
    })
  },

  validateLogin(req, res, next) {
    try {
      assert(typeof req.body.email === 'string', 'email must be a string.')
      assert(
        typeof req.body.password === 'string',
        'password must be a string.'
      )
      next()
    } catch (ex) {
      res
        .status(400)
        .json({ error: ex.toString(), datetime: new Date().toISOString() })
    }
  },

  register(req, res, next) {
    logger.info('register')
    logger.info(req.body.email)
    let email = req.body.email

    let testQuery = 'SELECT * FROM user' + (email ? ' WHERE Email = ?' : '')

    pool.getConnection((err, connection) => {
      if (err) {
        logger.error('Error getting connection from pool: ' + err.toString())
        res
          .status(500)
          .json({ error: ex.toString(), datetime: new Date().toISOString() })
      }

      if (connection) {
        connection.query(testQuery, [email], (error, results, fields) => {
          if (results.length > 0) {
            res.status(400).json({
              error: 'Account already exists',
              datetime: new Date().toISOString()
            })
          } else {
            let { firstname, lastname, email, studentnr, password } = req.body
            bcrypt.hash(password, saltRounds, function (err, hash) {
              connection.query(
                'INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES (?, ?, ?, ?, ?)',
                [firstname, lastname, email, studentnr, hash],
                (err, rows, fields) => {
                  connection.release()
                  if (err) {
                    logger.error('Error: ' + err.toString())
                    res.status(400).json({
                      error: 'Account already exists',
                      datetime: new Date().toISOString()
                    })
                  } else {
                    const payload = {
                      id: rows.insertId
                    }
                    const userinfo = {
                      token: jwt.sign(payload, 'secret', { expiresIn: '2h' }),
                      username: firstname + ' ' + lastname
                    }
                    res.status(200).json(userinfo)
                  }
                }
              )
            })
          }
        })
      }
    })
  },

  validateRegister(req, res, next) {
    try {
      assert(
        typeof req.body.firstname === 'string',
        'firstname must be a string.'
      )
      assert(
        typeof req.body.lastname === 'string',
        'lastname must be a string.'
      )
      assert(
        typeof req.body.studentnr === 'string',
        'studentnr must be a string.'
      )
      assert(typeof req.body.email === 'string', 'E-mail must be a string.')
      assert.match(
        req.body.email,
        /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/,
        'E-mail is not valid!'
      )
      assert(
        typeof req.body.password === 'string',
        'password must be a string.'
      )
      assert.match(
        req.body.password,
        /(?=.{7,})/,
        'Password must be at least 7 characters long!'
      )
      assert.match(
        req.body.studentnr,
        /^\d{7}$/,
        'Studentnumber has to be 7 numbers long!'
      )
      next()
    } catch (ex) {
      res
        .status(400)
        .json({ error: ex.toString(), datetime: new Date().toISOString() })
    }
  },

  validateToken(req, res, next) {
    logger.info('validateToken called')
    logger.trace(req.headers)
    const authHeader = req.headers.authorization
    if (!authHeader) {
      logger.warn('Authorization header missing!')
      res.status(401).json({
        error: 'Authorization header missing!',
        datetime: new Date().toISOString()
      })
    }
    const token = authHeader.substring(7, authHeader.length)

    jwt.verify(token, 'secret', (err, payload) => {
      if (err) {
        logger.warn('Not authorized')
        res.status(401).json({
          error: 'Not authorized',
          datetime: new Date().toISOString()
        })
      }
      if (payload) {
        logger.debug('token is valid', payload)
        req.userId = payload.id
        next()
      }
    })
  }
}
