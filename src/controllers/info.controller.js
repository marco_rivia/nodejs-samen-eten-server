const config = require('../config/logger')
const logger = config.logger

let controller = {
  getInfo(req, res, next) {
    logger.debug('Get request sent at /info')

    const info = {
      author: 'Marco',
      studentnumber: '2149386',
      description: 'Server can be used by students to find suitable meals',
      sonarqubeAnalysis:
        'https://sonarqube.avans-informatica-breda.nl/dashboard?id=programmeren-4'
    }

    res.status(200).json(info)
  }
}

module.exports = controller
