const config = require('../config/logger')
const logger = config.logger
const assert = require('assert')
const database = require('../config/database')
const pool = require('../config/database')

let controller = {
  validateMeal(req, res, next) {
    try {
      const {
        name,
        description,
        ingredients,
        allergies,
        price,
        maxparticipants
      } = req.body
      assert(typeof name === 'string', 'Name is missing!')
      assert(typeof description === 'string', 'Description is missing!')
      assert(typeof ingredients === 'string', 'Ingredients is missing!')
      assert(typeof allergies === 'string', 'Allergies is missing!')
      assert(typeof price === 'number', 'Price is missing!')
      assert(
        typeof maxparticipants === 'number',
        'Max participants is missing!'
      )
      next()
    } catch (err) {
      res.status(400).json({
        error: err.toString(),
        datetime: new Date().toISOString()
      })
    }
  },
  createMeal(req, res, next) {
    logger.debug('Post request sent at /studenthome/:homeId/meal')
    const meal = req.body
    let {
      name,
      description,
      ingredients,
      allergies,
      price,
      maxparticipants
    } = meal
    logger.debug('Meal = ', meal)

    const userId = req.userId
    const studenthomeId = req.params.homeId
    var date = new Date()

    let sqlQuery =
      'INSERT INTO `meal` (`Name`, `Description`, `Ingredients`, `Allergies`,`CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudenthomeID`, `MaxParticipants`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
    logger.debug('create Meal ', 'Query = ', sqlQuery)

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error('Createstudenthome ', error)
        res.status(400).json({
          error: err.toString(),
          datetime: new Date().toISOString()
        })
      }
      if (connection) {
        connection.query(
          sqlQuery,
          [
            name,
            description,
            ingredients,
            allergies,
            date,
            date,
            price,
            userId,
            studenthomeId,
            maxparticipants
          ],
          (error, results, fields) => {
            connection.release()
            if (error) {
              logger.error('create Meal', error)
              res.status(400).json({
                error: error.toString(),
                datetime: new Date().toISOString()
              })
            }
            if (results) {
              logger.debug('Results: ', results)
              res.status(200).json({
                result: meal,
                id: results.insertId
              })
            }
          }
        )
      }
    })
  },

  getMeals(req, res, next) {
    const homeId = req.params.homeId
    logger.info('get Meals called', 'HomeId =', homeId)

    let sqlQuery =
      'SELECT * FROM meal' + (homeId ? ' WHERE StudenthomeID = ?' : '')
    logger.debug('getAll', 'sqlQuery =', sqlQuery)

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          error: err.toString(),
          datetime: new Date().toISOString()
        })
      }

      // Use the connection
      connection.query(sqlQuery, [homeId], (error, results, fields) => {
        // When done with the connection, release it.
        connection.release()
        // Handle error after the release.
        if (error) {
          res.status(400).json({
            error: error.toString(),
            datetime: new Date().toISOString()
          })
        }
        if (results.length > 0) {
          logger.trace('Results: ', results)
          res.status(200).json({
            result: results
          })
        } else {
          logger.trace('No meals were found: ')
          res.status(400).json({
            error: 'No meals found for this studenthome',
            datetime: new Date().toISOString()
          })
        }
      })
    })
  },

  getMealById(req, res, next) {
    const homeId = req.params.homeId
    const mealId = req.params.mealId

    logger.info('Get meal by ID called', 'homeId =', homeId, 'mealId =', mealId)
    let sqlQuery =
      'SELECT * FROM meal' +
      (mealId ? ' WHERE ID = ? AND StudenthomeID = ?' : '')
    logger.debug('getAll', 'sqlQuery =', sqlQuery)

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          error: err.toString(),
          datetime: new Date().toISOString()
        })
      }

      // Use the connection
      connection.query(sqlQuery, [mealId, homeId], (error, results, fields) => {
        // When done with the connection, release it.
        connection.release()
        // Handle error after the release.
        if (error) {
          res.status(400).json({
            error: error.toString(),
            datetime: new Date().toISOString()
          })
        }
        if (results.length > 0) {
          logger.trace('Results: ', results)
          res.status(200).json({
            result: results
          })
        } else {
          logger.trace('No meals were found: ')
          res.status(400).json({
            error: 'No meal found with this id',
            datetime: new Date().toISOString()
          })
        }
      })
    })
  },
  updateMealById(req, res, next) {
    logger.info('Put request sent at /api/studenthome/')

    const meal = req.body
    const userId = req.userId

    logger.debug('update meal', 'userID = ', userId)

    let {
      name,
      description,
      ingredients,
      allergies,
      price,
      maxparticipants
    } = meal
    logger.debug('Meal = ', meal)

    const homeId = req.params.homeId
    const mealId = req.params.mealId

    logger.info('Update by id', 'homeId =', homeId)
    let sqlQuery =
      'UPDATE meal SET Name = "' +
      req.body.name +
      '", Description = "' +
      req.body.description +
      '", Ingredients = "' +
      req.body.ingredients +
      '", Allergies = "' +
      req.body.allergies +
      '", Price = "' +
      req.body.price +
      '", MaxParticipants = "' +
      req.body.maxparticipants +
      '"' +
      (homeId ? ' WHERE StudenthomeID = ? AND UserID = ? AND ID = ?' : '')
    logger.debug('Update meal by id', 'sqlQuery =', sqlQuery)
    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          error: err.toString(),
          datetime: new Date().toISOString()
        })
      }

      // Use the connection
      connection.query(
        sqlQuery,
        [homeId, userId, mealId],
        (error, results, fields) => {
          // When done with the connection, release it.
          connection.release()
          // Handle error after the release.
          if (error) {
            res.status(400).json({
              error: error.toString(),
              datetime: new Date().toISOString()
            })
          }
          if (results) {
            if (results.affectedRows === 0) {
              logger.trace('item was NOT updated')
              res.status(401).json({
                result: {
                  error:
                    'Item not found of you do not have access to this item',
                  datetime: new Date().toISOString()
                }
              })
              return
            }
            res.status(200).json({
              result: meal
            })
          }
        }
      )
    })
  },

  deleteMealById(req, res, next) {
    const homeId = req.params.homeId
    const mealId = req.params.mealId
    const userId = req.userId

    logger.info(
      'Delete meal by ID called',
      'homeId =',
      homeId,
      'mealId =',
      mealId
    )
    let sqlQuery =
      'DELETE FROM meal' +
      (mealId ? ' WHERE ID = ? AND StudenthomeID = ? AND UserID = ?' : '')

    let deleteParticipantsFromMeal =
      'DELETE FROM participants' + (mealId ? ' where MealID = ?' : '')

    let sqlQuery2 = 'SELECT * FROM meal' + (homeId ? ' WHERE ID = ?' : '')
    logger.debug('getAll', 'sqlQuery =', sqlQuery)

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          error: err.toString(),
          datetime: new Date().toISOString()
        })
      }

      connection.query(sqlQuery2, [mealId], (error, results, fields) => {
        if (error) {
          res.status(400).json({
            error: error.toString(),
            datetime: new Date().toISOString()
          })
        }
        if (results) {
          logger.trace('Results: ', results)
          const meal = results
          connection.query(
            deleteParticipantsFromMeal,
            [mealId],
            (error, results, fields) => {
              if (error) {
                res.status(400).json({
                  error: error.toString(),
                  datetime: new Date().toISOString()
                })
              }
              if (results) {
                connection.query(
                  sqlQuery,
                  [mealId, homeId, userId],
                  (error, results, fields) => {
                    connection.release()
                    if (error) {
                      res.status(400).json({
                        error: error.toString(),
                        datetime: new Date().toISOString()
                      })
                    }
                    if (results) {
                      if (results.affectedRows === 0) {
                        logger.trace('Item was not deleted')
                        res.status(401).json({
                          result: {
                            error:
                              'Item not found of you do not have access to this item',
                            datetime: new Date().toISOString()
                          }
                        })
                        return
                      }
                      res.status(200).json({
                        result: meal
                      })
                    }
                  }
                )
              }
            }
          )
        }
      })
    })
  }
}

module.exports = controller
