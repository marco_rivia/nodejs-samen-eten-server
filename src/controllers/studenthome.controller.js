const config = require('../config/logger')
const logger = config.logger
const assert = require('assert')
const database = require('../config/database')
const pool = require('../config/database')

let controller = {
  validateStudenthome(req, res, next) {
    try {
      const { name, address, housenr, postalcode, telephone, city } = req.body
      assert(typeof name === 'string', 'Name is missing!')
      assert(typeof address === 'string', 'Address is missing!')
      assert(typeof housenr === 'string', 'Housenr is missing!')
      assert(typeof postalcode === 'string', 'Postalcode is missing!')
      assert(typeof telephone === 'string', 'Telephone is missing!')
      assert(typeof city === 'string', 'City is missing!')
      assert.match(
        postalcode,
        /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/,
        'Postalcode is not valid!'
      )
      assert.match(telephone, /^06(\s|-)?\d{8}$/, 'Phonenumber is not valid!')
      next()
    } catch (err) {
      res.status(400).json({
        error: err.toString(),
        datetime: new Date().toISOString()
      })
    }
  },

  validateStudenthomeUser(req, res, next) {
    try {
      const { userid } = req.body
      assert(typeof userid === 'string', 'UserID is missing!')
      next()
    } catch (err) {
      res.status(400).json({
        error: err.toString(),
        datetime: new Date().toISOString()
      })
    }
  },

  createStudenthome(req, res, next) {
    logger.debug('Post request sent at /api/studenthome')
    const studenthome = req.body
    let { name, address, housenr, postalcode, telephone, city } = studenthome
    logger.debug('Studenthome = ', studenthome)

    const userId = req.userId

    let testQuery =
      'SELECT * FROM studenthome' + (address ? ' WHERE Address = ?' : '')

    let sqlQuery =
      'INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`,`Postal_Code`, `Telephone`, `City`) VALUES (?, ?, ?, ?, ?, ?, ?)'
    logger.debug('createStudenthome ', 'Query = ', sqlQuery)

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error('Createstudenthome ', error)
        res.status(400).json({
          error: err.toString(),
          datetime: new Date().toISOString()
        })
      }
      if (connection) {
        connection.query(testQuery, [address], (error, results, fields) => {
          if (results.length > 0) {
            res.status(400).json({
              error: 'Studenthome already exists',
              datetime: new Date().toISOString()
            })
          } else {
            connection.query(
              sqlQuery,
              [name, address, housenr, userId, postalcode, telephone, city],
              (error, results, fields) => {
                connection.release()
                if (error) {
                  logger.error('CreateStudenthome', error)
                  res.status(400).json({
                    error: error.toString(),
                    datetime: new Date().toISOString()
                  })
                }
                if (results) {
                  logger.trace('Results: ', results)
                  res.status(200).json({
                    result: studenthome,
                    id: results.insertId
                  })
                }
              }
            )
          }
        })
      }
    })
  },

  getStudenthomes(req, res, next) {
    const studenthome = req.query.studenthome
    const city = req.query.city
    const name = req.query.name

    logger.info('getAll', 'studenthome =', studenthome)
    logger.info('getAll', 'city =', city)
    logger.info('getAll', 'name =', name)

    if (name && city) {
      logger.debug('Filtering name and city')
      pool.getConnection(function (err, connection) {
        if (err) {
          res.status(400).json({
            error: err.toString(),
            datetime: new Date().toISOString()
          })
        }
        let sqlQuery =
          'SELECT * FROM studenthome' +
          (name ? ' WHERE Name LIKE ? AND City = ?' : '')
        logger.debug('Query = ', sqlQuery)
        connection.query(
          sqlQuery,
          [name + '%', city],
          (error, results, fields) => {
            connection.release()
            if (error) {
              res.status(400).json({
                error: error.toString(),
                datetime: new Date().toISOString()
              })
            }
            if (results.length > 0) {
              logger.trace('Results: ', results)
              res.status(200).json({
                result: results
              })
            } else {
              res.status(400).json({
                error: 'No studenthomes found',
                datetime: new Date().toISOString()
              })
            }
          }
        )
      })
      return
    }

    if (city) {
      logger.debug('Filtering city')
      pool.getConnection(function (err, connection) {
        if (err) {
          res.status(400).json({
            error: err.toString(),
            datetime: new Date().toISOString()
          })
        }
        let sqlQuery =
          'SELECT * FROM studenthome' + (city ? ' WHERE City = ?' : '')
        connection.query(sqlQuery, [city], (error, results, fields) => {
          connection.release()
          if (error) {
            res.status(400).json({
              error: error.toString(),
              datetime: new Date().toISOString()
            })
          }
          if (results.length > 0) {
            logger.trace('Results: ', results)
            res.status(200).json({
              result: results
            })
          } else {
            res.status(400).json({
              error: 'No studenthomes found',
              datetime: new Date().toISOString()
            })
          }
        })
      })
      return
    }

    if (name) {
      logger.debug('Filtering name')
      pool.getConnection(function (err, connection) {
        if (err) {
          res.status(400).json({
            error: err.toString(),
            datetime: new Date().toISOString()
          })
        }
        let sqlQuery =
          'SELECT * FROM studenthome' + (name ? ' WHERE Name LIKE ?' : '')
        connection.query(sqlQuery, [name + '%'], (error, results, fields) => {
          connection.release()
          if (error) {
            res.status(400).json({
              error: error.toString(),
              datetime: new Date().toISOString()
            })
          }
          if (results.length > 0) {
            logger.trace('Results: ', results)
            res.status(200).json({
              result: results
            })
          } else {
            res.status(400).json({
              error: 'No studenthomes found',
              datetime: new Date().toISOString()
            })
          }
        })
      })
      return
    }

    logger.debug('Filtering nothing')
    let sqlQuery =
      'SELECT * FROM studenthome' +
      (studenthome ? ' WHERE Studenthome = ?' : '')
    logger.debug('getAll', 'sqlQuery =', sqlQuery)

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          error: err.toString(),
          datetime: new Date().toISOString()
        })
      }

      // Use the connection
      connection.query(sqlQuery, [studenthome], (error, results, fields) => {
        // When done with the connection, release it.
        connection.release()
        // Handle error after the release.
        if (error) {
          res.status(400).json({
            error: error.toString(),
            datetime: new Date().toISOString()
          })
        }
        if (results.length > 0) {
          logger.trace('Results: ', results)
          res.status(200).json({
            result: results
          })
        } else {
          res.status(400).json({
            error: 'No studenthomes found',
            datetime: new Date().toISOString()
          })
        }
      })
    })
  },

  getStudenthomeById(req, res, next) {
    const homeId = req.params.homeId
    logger.info('getByID', 'homeId =', homeId)
    let sqlQuery = 'SELECT * FROM studenthome' + (homeId ? ' WHERE ID = ?' : '')
    logger.debug('getAll', 'sqlQuery =', sqlQuery)

    let sqlQueryUser =
      'SELECT * FROM studenthome_user' +
      (homeId ? ' WHERE StudenthomeID = ?' : '')

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          error: err.toString(),
          datetime: new Date().toISOString()
        })
      }

      // Use the connection
      connection.query(sqlQuery, [homeId], (error, results, fields) => {
        // When done with the connection, release it.
        // Handle error after the release.
        if (error) {
          res.status(400).json({
            error: error.toString(),
            datetime: new Date().toISOString()
          })
        }
        if (results.length > 0) {
          logger.trace('Results: ', results)
          const studenthome = results
          let sqlQuery2 =
            'SELECT * FROM meal' + (homeId ? ' WHERE StudenthomeID = ?' : '')
          connection.query(sqlQuery2, [homeId], (error, results, fields) => {
            // Handle error after the release.
            if (error) {
              res.status(400).json({
                error: error.toString(),
                datetime: new Date().toISOString()
              })
            }
            if (results) {
              const meals = results
              connection.query(
                sqlQueryUser,
                [homeId],
                (error, results, fields) => {
                  connection.release()
                  if (error) {
                    res.status(400).json({
                      error: error.toString(),
                      datetime: new Date().toISOString()
                    })
                  }
                  if (results) {
                    res.status(200).json({
                      result: studenthome,
                      meals: meals,
                      users: results
                    })
                  }
                }
              )
            }
          })
        } else {
          res.status(400).json({
            error: 'No studenthomes found',
            datetime: new Date().toISOString()
          })
        }
      })
    })
  },

  updateStudenthomeById(req, res, next) {
    logger.info('Put request sent at /api/studenthome/')
    const homeId = req.params.homeId
    const studenthome = req.body
    const userId = req.userId
    logger.info('getByID', 'homeId =', homeId)
    let sqlQuery =
      'UPDATE studenthome SET Name = "' +
      req.body.name +
      '", Address = "' +
      req.body.address +
      '", House_Nr = "' +
      req.body.housenr +
      '", Postal_Code = "' +
      req.body.postalcode +
      '", Telephone = "' +
      req.body.telephone +
      '", City = "' +
      req.body.city +
      '"' +
      (homeId ? ' WHERE ID = ? AND UserId = ?' : '')
    logger.debug('Update by id', 'sqlQuery =', sqlQuery)
    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          error: err.toString(),
          datetime: new Date().toISOString()
        })
      }

      // Use the connection
      connection.query(sqlQuery, [homeId, userId], (error, results, fields) => {
        // When done with the connection, release it.
        connection.release()
        // Handle error after the release.
        if (error) {
          res.status(400).json({
            error: error.toString(),
            datetime: new Date().toISOString()
          })
        }
        if (results) {
          if (results.affectedRows === 0) {
            logger.trace('item was NOT updated')
            res.status(401).json({
              result: {
                error: 'Item not found of you do not have access to this item',
                datetime: new Date().toISOString()
              }
            })
            return
          }
          res.status(200).json({
            result: studenthome
          })
        }
      })
    })
  },

  deleteStudenthomeById(req, res, next) {
    const homeId = req.params.homeId
    const userId = req.userId
    logger.info('Delete by id', 'homeId =', homeId)
    logger.info('Delete by id', 'userId =', userId)

    let sqlUserQuery =
      'DELETE FROM studenthome_user' +
      (homeId ? ' WHERE StudenthomeID = ?' : '')
    let sqlQuery =
      'DELETE FROM studenthome' + (homeId ? ' WHERE ID = ? AND UserID = ?' : '')
    let sqlQuery2 =
      'SELECT * FROM studenthome' + (homeId ? ' WHERE ID = ?' : '')
    logger.debug('Delete by id', 'sqlQuery =', sqlQuery)

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          error: err.toString(),
          datetime: new Date().toISOString()
        })
      }

      connection.query(sqlQuery2, [homeId], (error, results, fields) => {
        if (error) {
          res.status(400).json({
            error: error.toString(),
            datetime: new Date().toISOString()
          })
        }
        if (results) {
          logger.trace('Results: ', results)
          const studenthome = results
          connection.query(
            sqlQuery,
            [homeId, userId],
            (error, results, fields) => {
              if (error) {
                res.status(400).json({
                  error: error.toString(),
                  datetime: new Date().toISOString()
                })
              }
              if (results) {
                if (results.affectedRows === 0) {
                  logger.trace('item was NOT deleted')
                  res.status(401).json({
                    result: {
                      error:
                        'Item not found of you do not have access to this item',
                      datetime: new Date().toISOString()
                    }
                  })
                  return
                }
                connection.query(
                  sqlUserQuery,
                  [homeId],
                  (error, results, fields) => {
                    if (error) {
                      res.status(400).json({
                        error: error.toString(),
                        datetime: new Date().toISOString()
                      })
                    }
                    if (results) {
                      res.status(200).json({
                        result: studenthome
                      })
                    }
                  }
                )
              }
            }
          )
        }
      })
    })
  },

  addUserToStudenthome(req, res, next) {
    const homeId = req.params.homeId
    const userIdFromJWT = req.userId

    const userId = req.body.userid

    logger.info('Add user to studenthome', 'homeId =', homeId)
    logger.info('Add user to studenthome', 'userId =', userId)

    let testQuery =
      'SELECT * FROM studenthome_user' +
      (userId ? ' WHERE UserID = ? AND StudenthomeID = ?' : '')

    let testQuery2 = 'SELECT * FROM user' + (userId ? ' WHERE ID = ?' : '')

    let testStudenthomeQuery =
      'SELECT * FROM studenthome' + (homeId ? ' WHERE ID = ?' : '')

    let sqlQuery =
      'INSERT INTO `studenthome_user` (`UserID`, `StudenthomeID`) VALUES (?, ?)'
    logger.debug('Add user to studenthome', 'Query = ', sqlQuery)

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error('Add user to studenthome', error)
        res.status(400).json({
          error: 'Add user to studenthome failed getting connection!',
          datetime: new Date().toISOString()
        })
      }
      if (connection) {
        connection.query(
          testQuery,
          [userId, homeId],
          (error, results, fields) => {
            if (results.length > 0) {
              res.status(400).json({
                error: 'User has already been added to studenthome',
                datetime: new Date().toISOString()
              })
            } else {
              connection.query(
                testQuery2,
                [userId],
                (error, results, fields) => {
                  if (results.length == 0) {
                    res.status(400).json({
                      error: 'User does not exist',
                      datetime: new Date().toISOString()
                    })
                  } else {
                    connection.query(
                      testStudenthomeQuery,
                      [homeId],
                      (error, results, fields) => {
                        if (results.length == 0) {
                          res.status(400).json({
                            error: 'Studenthome was not found',
                            datetime: new Date().toISOString()
                          })
                        } else {
                          connection.query(
                            sqlQuery,
                            [userId, homeId],
                            (error, results, fields) => {
                              connection.release()
                              if (error) {
                                logger.error('Add user to studenthome', error)
                                res.status(400).json({
                                  error: error.toString(),
                                  datetime: new Date().toISOString()
                                })
                              }
                              if (results) {
                                logger.trace('Results: ', results)
                                res.status(200).json({
                                  result: 'User has been added succesfully',
                                  UserID: userId,
                                  StudenthomeID: homeId
                                })
                              }
                            }
                          )
                        }
                      }
                    )
                  }
                }
              )
            }
          }
        )
      }
    })
  }
}

module.exports = controller
