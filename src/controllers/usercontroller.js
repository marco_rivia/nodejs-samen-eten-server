const config = require('../config/logger')
const logger = config.logger
const assert = require('assert')
const database = require('../config/database')
const pool = require('../config/database')

let controller = {
  signUpForMeal(req, res, next) {
    const userid = req.userId
    const homeid = req.params.homeId
    const mealid = req.params.mealId

    logger.log('Sign up for meal with userid: ', userid)

    let sqlQuery =
      'INSERT INTO participants (UserID, StudenthomeID, MealID, SignedUpOn) VALUES (?, ?, ?, ?)'

    let testQuery = 'SELECT * FROM user' + (userid ? ' WHERE ID = ?' : '')

    logger.debug('Query = ', sqlQuery)
    var date = new Date()

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error('Sign up for meal ', error)
        res.status(400).json({
          error: err.toString(),
          datetime: new Date().toISOString()
        })
      }
      if (connection) {
        connection.query(testQuery, [userid], (error, results, fields) => {
          if (results.length == 0) {
            res.status(400).json({
              error: 'User does not exist',
              datetime: new Date().toISOString()
            })
          } else {
            connection.query(
              sqlQuery,
              [userid, homeid, mealid, date],
              (error, results, fields) => {
                connection.release()
                if (error) {
                  logger.error('Sign up for meal', error)
                  res.status(400).json({
                    error: error.toString(),
                    datetime: new Date().toISOString()
                  })
                }
                if (results) {
                  logger.trace('Results: ', results)
                  res.status(200).json({
                    result: 'Participant was added',
                    UserID: userid,
                    StudenthomeID: homeid,
                    MealID: mealid
                  })
                }
              }
            )
          }
        })
      }
    })
  },

  cancelSignUpForMeal(req, res, next) {
    const userid = req.userId

    const homeid = req.params.homeId
    const mealid = req.params.mealId
    var date = new Date()

    logger.log('Sign off for meal with userid: ', userid)

    let sqlQuery =
      'DELETE FROM participants' +
      (userid ? ' WHERE UserID = ? AND StudenthomeID = ? AND MealID = ?' : '')
    logger.debug('Query = ', sqlQuery)

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error('Sign up for meal ', error)
        res.status(400).json({
          error: err.toString(),
          datetime: new Date().toISOString()
        })
      }
      if (connection) {
        connection.query(
          sqlQuery,
          [userid, homeid, mealid],
          (error, results, fields) => {
            connection.release()
            if (error) {
              logger.error('Cancel Sign up for meal', error)
              res.status(400).json({
                error: error.toString(),
                datetime: new Date().toISOString()
              })
            }
            if (results) {
              if (results.affectedRows === 0) {
                logger.trace('item was NOT updated')
                res.status(400).json({
                  result: {
                    error:
                      'Item not found of you do not have access to this item',
                    datetime: new Date().toISOString()
                  }
                })
                return
              }
              res.status(200).json({
                result: 'Participant was removed',
                UserID: userid,
                StudenthomeID: homeid,
                MealID: mealid
              })
            }
          }
        )
      }
    })
  },

  getParticipants(req, res, next) {
    const mealid = req.params.mealId
    const userid = req.userId

    logger.log('Get meal with mealid: ', mealid)

    let sqlQuery =
      'SELECT * FROM participants' + (mealid ? ' WHERE MealID = ?' : '')
    logger.debug('Query = ', sqlQuery)

    let testQuery =
      'SELECT * FROM meal' + (mealid ? ' WHERE ID = ? AND UserID = ?' : '')
    logger.debug('Test query = ', testQuery)

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error('Get meals ', error)
        res.status(400).json({
          error: err.toString(),
          datetime: new Date().toISOString()
        })
      }
      if (connection) {
        connection.query(
          testQuery,
          [mealid, userid],
          (error, results, fields) => {
            if (results.length == 0) {
              res.status(400).json({
                error: 'Not authorized!',
                datetime: new Date().toISOString()
              })
            } else {
              connection.query(sqlQuery, [mealid], (error, results, fields) => {
                connection.release()
                if (error) {
                  logger.error('Get meals', error)
                  res.status(400).json({
                    error: error.toString(),
                    datetime: new Date().toISOString()
                  })
                }
                if (results.length > 0) {
                  if (results.affectedRows === 0) {
                    res.status(401).json({
                      result: {
                        error:
                          'Item not found of you do not have access to this item',
                        datetime: new Date().toISOString()
                      }
                    })
                    return
                  }
                  res.status(200).json({
                    result: results
                  })
                } else {
                  res.status(400).json({
                    error: 'No participants found',
                    datetime: new Date().toISOString()
                  })
                }
              })
            }
          }
        )
      }
    })
  },

  getParticipantDetail(req, res, next) {
    const mealid = req.params.mealId
    const participantiId = req.params.participantiId
    const userid = req.userId

    logger.log('Details asked for user ', participantiId)

    let sqlQuery =
      'SELECT * FROM user' + (participantiId ? ' WHERE ID = ?' : '')
    logger.debug('Query = ', sqlQuery)

    let testQuery =
      'SELECT * FROM meal' + (mealid ? ' WHERE ID = ? AND UserID = ?' : '')
    logger.debug('Test query = ', testQuery)

    let testQuery2 =
      'SELECT * FROM participants' +
      (participantiId ? ' WHERE UserID = ? AND MealID = ? ' : '')

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error('Details asked for user ', error)
        res.status(400).json({
          error: err.toString(),
          datetime: new Date().toISOString()
        })
      }
      if (connection) {
        connection.query(
          testQuery,
          [mealid, userid],
          (error, results, fields) => {
            if (results.length == 0) {
              res.status(400).json({
                error: 'Not authorized!',
                datetime: new Date().toISOString()
              })
            } else {
              connection.query(
                testQuery2,
                [participantiId, mealid],
                (error, results, fields) => {
                  if (results.length > 0) {
                    console.log(results)
                    connection.query(
                      sqlQuery,
                      [participantiId],
                      (error, results, fields) => {
                        connection.release()
                        if (error) {
                          logger.error('Details asked for user', error)
                          res.status(400).json({
                            error: error.toString(),
                            datetime: new Date().toISOString()
                          })
                        }
                        if (results.length > 0) {
                          logger.debug('Results: ', results)
                          res.status(200).json({
                            ID: results[0].ID,
                            Name:
                              results[0].First_Name + '' + results[0].Last_Name,
                            Email: results[0].Email,
                            Studentnumber: results[0].Student_Number
                          })
                        } else {
                          res.status(400).json({
                            error: 'Participant was not found',
                            datetime: new Date().toISOString()
                          })
                        }
                      }
                    )
                  } else {
                    res.status(400).json({
                      error: 'Participant was not found',
                      datetime: new Date().toISOString()
                    })
                  }
                }
              )
            }
          }
        )
      }
    })
  }
}

module.exports = controller
