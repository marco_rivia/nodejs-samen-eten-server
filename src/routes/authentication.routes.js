const router = require('express').Router()
const AuthController = require('../controllers/authentication.controller')

router.post('/login', AuthController.validateLogin, AuthController.login)
router.post(
  '/register',
  AuthController.validateRegister,
  AuthController.register
)

module.exports = router
