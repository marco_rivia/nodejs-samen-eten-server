const infocontroller = require('../controllers/info.controller')
const router = require('express').Router()

router.get('/info', infocontroller.getInfo)

module.exports = router
