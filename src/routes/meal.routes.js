const router = require('express').Router()
const mealcontroller = require('../controllers/mealcontroller')
const AuthController = require('../controllers/authentication.controller')

router.post(
  '/studenthome/:homeId/meal',
  AuthController.validateToken,
  mealcontroller.validateMeal,
  mealcontroller.createMeal
)
router.get('/studenthome/:homeId/meal/:mealId', mealcontroller.getMealById)
router.get('/studenthome/:homeId/meal', mealcontroller.getMeals)
router.put(
  '/studenthome/:homeId/meal/:mealId',
  AuthController.validateToken,
  mealcontroller.validateMeal,
  mealcontroller.updateMealById
)
router.delete(
  '/studenthome/:homeId/meal/:mealId',
  AuthController.validateToken,
  mealcontroller.deleteMealById
)

module.exports = router
