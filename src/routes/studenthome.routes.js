const router = require('express').Router()
const studenthomecontroller = require('../controllers/studenthome.controller')
const AuthController = require('../controllers/authentication.controller')

router.post(
  '/studenthome',
  AuthController.validateToken,
  studenthomecontroller.validateStudenthome,
  studenthomecontroller.createStudenthome
)
router.get('/studenthome', studenthomecontroller.getStudenthomes)
router.get('/studenthome/:homeId', studenthomecontroller.getStudenthomeById)
router.put(
  '/studenthome/:homeId',
  AuthController.validateToken,
  studenthomecontroller.validateStudenthome,
  studenthomecontroller.updateStudenthomeById
)
router.put(
  '/studenthome/:homeId/user',
  AuthController.validateToken,
  studenthomecontroller.validateStudenthomeUser,
  studenthomecontroller.addUserToStudenthome
)
router.delete(
  '/studenthome/:homeId',
  AuthController.validateToken,
  studenthomecontroller.deleteStudenthomeById
)

module.exports = router
