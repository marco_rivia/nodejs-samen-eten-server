const router = require('express').Router()
const usercontroller = require('../controllers/usercontroller')
const AuthController = require('../controllers/authentication.controller')

router.post(
  '/studenthome/:homeId/meal/:mealId/signup',
  AuthController.validateToken,
  usercontroller.signUpForMeal
)
router.put(
  '/studenthome/:homeId/meal/:mealId/signoff',
  AuthController.validateToken,
  usercontroller.cancelSignUpForMeal
)
router.get(
  '/meal/:mealId/participants',
  AuthController.validateToken,
  usercontroller.getParticipants
)

router.get(
  '/meal/:mealId/participants/:participantiId',
  AuthController.validateToken,
  usercontroller.getParticipantDetail
)

module.exports = router
