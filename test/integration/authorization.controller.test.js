process.env.DB_DATABASE = process.env.DB_DATABASE || '2149386'
process.env.NODE_ENV = 'testing'
console.log(`Running tests using database '${process.env.DB_DATABASE}'`)

const pool = require('../../src/config/database')
const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../server')
const jwt = require('jsonwebtoken')

chai.should()
chai.use(chaiHttp)

const INSERT_USER =
  'INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES' +
  '("first", "last", "name@server.nl","1234567", "secretpass");'

const CLEAR_USER_TABLE = 'DELETE IGNORE FROM `user`'
const CLEAR_STUDENTHOME_TABLE = 'DELETE IGNORE FROM `studenthome`'
const CLEAR_PARTICIPANTS_TABLE = 'DELETE IGNORE FROM `participants`'

before((done) => {
  pool.query(INSERT_USER, (err, rows, fields) => {
    if (err) {
      console.log(`before INSERT_USER: ${err}`)
      done(err)
    } else {
      insertId = rows.insertId
      done()
    }
  })
})

before((done) => {
  pool.query(CLEAR_STUDENTHOME_TABLE, (err, rows, fields) => {
    if (err) {
      console.log(`beforeEach CLEAR_STUDENTHOME_TABLE: ${err}`)
      done(err)
    } else {
      done()
    }
  })
})

after((done) => {
  pool.query(CLEAR_USER_TABLE, (err, rows, fields) => {
    if (err) {
      console.log(`after error: ${err}`)
      done(err)
    } else {
      done()
    }
  })
})

describe('Manage authentication', () => {
  describe('UC101 Manage register - POST /api/register', () => {
    it('TC-101-1', (done) => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          lastname: 'tramper',
          email: 'email@email.nl',
          studentnr: '1234567',
          password: 'secretpass'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('error').which.is.a('string')
          done()
        })
    })

    it('TC-101-2', (done) => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'marco',
          lastname: 'tramper',
          email: 'email@emailisnotvalid',
          studentnr: '1234567',
          password: 'secretpass'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('error').which.is.a('string')
          done()
        })
    })

    it('TC-101-3', (done) => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'marco',
          lastname: 'tramper',
          email: 'email@email.nl',
          studentnr: '1234567',
          password: 'nv'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('error').which.is.a('string')
          done()
        })
    })

    it('TC-101-4', (done) => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'first',
          lastname: 'last',
          email: 'name@server.nl',
          studentnr: '1234567',
          password: 'secretpassword'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('error').which.is.a('string')
          done()
        })
    })

    it('TC-101-5', (done) => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'marco',
          lastname: 'tramper',
          email: 'testUser@test.nl',
          phonenumber: '0612345678',
          studentnr: '1234567',
          password: 'secretpassword'
        })
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('token').which.is.a('string')
          response.should.have.property('username').which.is.a('string')
          done()
        })
    })
  })
}),
  describe('UC102 Manage login - POST /api/login', () => {
    it('TC-102-1', (done) => {
      chai
        .request(server)
        .post('/api/login')
        .send({
          email: 'mdw@h.nl'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('error').which.is.a('string')
          done()
        })
    })

    it('TC-102-2', (done) => {
      chai
        .request(server)
        .post('/api/login')
        .send({
          email: 'anInvalidEmail@email',
          Password: 'password'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('error').which.is.a('string')
          done()
        })
    })

    it('TC-102-3', (done) => {
      chai
        .request(server)
        .post('/api/login')
        .send({
          email: 'email@email.nl',
          Password: 'inv'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('error').which.is.a('string')
          done()
        })
    })

    it('TC-102-4', (done) => {
      chai
        .request(server)
        .post('/api/login')
        .send({
          email: 'nonexisting@email.nl',
          Password: 'password'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('error').which.is.a('string')
          done()
        })
    })

    it('TC-102-5', (done) => {
      chai
        .request(server)
        .post('/api/login')
        .send({
          email: 'testUser@test.nl',
          password: 'secretpassword'
        })
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('token').which.is.a('string')
          response.should.have.property('username').which.is.a('string')
          done()
        })
    })
  })
