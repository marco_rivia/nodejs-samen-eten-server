process.env.DB_DATABASE = process.env.DB_DATABASE || '2149386'
process.env.NODE_ENV = 'testing'
console.log(`Running tests using database '${process.env.DB_DATABASE}'`)

const pool = require('../../src/config/database')
const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../server')
const jwt = require('jsonwebtoken')

chai.should()
chai.use(chaiHttp)

const CLEAR_STUDENTHOME_TABLE = 'DELETE IGNORE FROM `studenthome`'
const CLEAR_USER_TABLE = 'DELETE IGNORE FROM `user`'
const CLEAR_PARTICIPANTS_TABLE = 'DELETE IGNORE FROM `participants`'
const CLEAR_STUDENTHOME_USER_TABLE = 'DELETE IGNORE FROM `studenthome_user`'

const INSERT_USER =
  'INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES' +
  '("first", "last", "namee@server.nl","1234567", "secret");'
let insertId

const INSERT_STUDENTHOME =
  'INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`,`Postal_Code`, `Telephone`, `City`) VALUES ("Name", "Address", "1", ? ,"4463EA", "0640883399", "Goes")'
let insertIdStudenthome

before((done) => {
  pool.query(INSERT_USER, (err, rows, fields) => {
    if (err) {
      console.log(`before INSERT_USER: ${err}`)
      done(err)
    } else {
      insertId = rows.insertId
      done()
    }
  })
})

before((done) => {
  pool.query(CLEAR_STUDENTHOME_TABLE, (err, rows, fields) => {
    if (err) {
      console.log(`beforeEach CLEAR_STUDENTHOME_TABLE: ${err}`)
      done(err)
    } else {
      done()
    }
  })
})

before((done) => {
  pool.query(INSERT_STUDENTHOME, [insertId], (err, rows, fields) => {
    if (err) {
      console.log(`beforeEach CLEAR_STUDENTHOME_TABLE: ${err}`)
      done(err)
    } else {
      insertIdStudenthome = rows.insertId
      done()
    }
  })
})

after((done) => {
  pool.query(CLEAR_PARTICIPANTS_TABLE, (err, rows, fields) => {
    if (err) {
      console.log(`after error: ${err}`)
      done(err)
    } else {
      done()
    }
  })
})

after((done) => {
  pool.query(CLEAR_USER_TABLE, (err, rows, fields) => {
    if (err) {
      console.log(`after error: ${err}`)
      done(err)
    } else {
      done()
    }
  })
})

after((done) => {
  pool.query(CLEAR_STUDENTHOME_USER_TABLE, (err, rows, fields) => {
    if (err) {
      console.log(`after error: ${err}`)
      done(err)
    } else {
      done()
    }
  })
})

describe('Manage studenthomes - POST /api/studenthome', () => {
  describe('UC201 Create studenthome', () => {
    it('TC-201-1', (done) => {
      chai
        .request(server)
        .post('/api/studenthome')
        .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
        .send({
          name: 'aName',
          address: 'anAddress'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('error').which.is.a('string')
          done()
        })
    })

    it('TC-201-2', (done) => {
      chai
        .request(server)
        .post('/api/studenthome')
        .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
        .send({
          name: 'marco',
          address: 'leuvenaarvvstraat2',
          housenr: '52vvB',
          postalcode: '446TEST',
          telephone: '0640883399',
          city: 'Breda'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('error').which.is.a('string')
          done()
        })
    })

    it('TC-201-3', (done) => {
      chai
        .request(server)
        .post('/api/studenthome')
        .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
        .send({
          name: 'marco',
          address: 'leuvenaarvvstraat2',
          housenr: '52vvB',
          postalcode: '4463VK',
          telephone: '064099TEST',
          city: 'Breda'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('error').which.is.a('string')
          done()
        })
    })

    it('TC-201-4', (done) => {
      chai
        .request(server)
        .post('/api/studenthome')
        .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
        .send({
          name: 'Name',
          address: 'Address',
          housenr: '1',
          postalcode: '4463EA',
          telephone: '0640883399',
          city: 'Goes'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          let { error } = res.body
          error.should.be.an('string')

          done()
        })
    })

    it('TC-201-5', (done) => {
      chai
        .request(server)
        .post('/api/studenthome')
        .send({
          name: 'Name',
          address: 'NewAddress',
          housenr: '1B',
          postalcode: '4463EA',
          telephone: '0640883399',
          city: 'Breda'
        })
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')

          let { error } = res.body
          error.should.be.an('string')

          done()
        })
    })

    it('TC-201-6', (done) => {
      chai
        .request(server)
        .post('/api/studenthome')
        .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
        .send({
          name: 'nametesting',
          address: 'streetfortestingg',
          housenr: '52T',
          postalcode: '4461TT',
          telephone: '0640883399',
          city: 'cityTest'
        })
        .end((err, res) => {
          res.should.have.status(400)
          done()
        })
    })
  })

  describe('UC202 List studenthomes - GET /api/studenthome', () => {
    it('TC-202-1', (done) => {
      chai
        .request(server)
        .get('/api/studenthome?city=jfejfeu')
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          let { error } = res.body
          error.should.be.an('string')

          done()
        })
    })

    it('TC-202-2', (done) => {
      chai
        .request(server)
        .get('/api/studenthome')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body
          result.should.be.an('array').that.has.length(1)

          done()
        })
    })

    it('TC-202-3', (done) => {
      chai
        .request(server)
        .get('/api/studenthome?city=jfejfeu')
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          let { error } = res.body
          error.should.be.an('string')

          done()
        })
    })

    it('TC-202-4', (done) => {
      chai
        .request(server)
        .get('/api/studenthome?name=hfuhuiehif')
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          let { error } = res.body
          error.should.be.an('string')

          done()
        })
    })

    it('TC-202-5', (done) => {
      chai
        .request(server)
        .get('/api/studenthome?city=Goes')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body
          result.should.be.an('array').that.has.length(1)

          done()
        })
    })

    it('TC-202-6', (done) => {
      chai
        .request(server)
        .get('/api/studenthome?name=Name')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body
          result.should.be.an('array').that.has.length(1)

          done()
        })
    })
  })
}),
  describe('UC203 Detail studenthomes - GET /api/studenthome:homeId', () => {
    it('TC-203-1', (done) => {
      chai
        .request(server)
        .get('/api/studenthome/10000')
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          let { error } = res.body
          error.should.be.an('string')

          done()
        })
    })

    it('TC-203-2', (done) => {
      chai
        .request(server)
        .get('/api/studenthome/' + insertIdStudenthome)
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          let { error } = res.body
          error.should.be.an('string')

          done()
        })
    })
  }),
  describe('UC204 Update studenthomes - PUT /api/studenthome:homeId', () => {
    it('TC-204-1', (done) => {
      chai
        .request(server)
        .put('/api/studenthome/10000')
        .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
        .send({
          name: 'aName',
          address: 'anAddress'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('error').which.is.a('string')
          done()
        })
    })

    it('TC-204-2', (done) => {
      chai
        .request(server)
        .put('/api/studenthome/10000')
        .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
        .send({
          name: 'marco',
          address: 'leuvenaarvvstraat2',
          housenr: '52vvB',
          postalcode: '446TEST',
          telephone: '0640883399',
          city: 'Breda'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('error').which.is.a('string')
          done()
        })
    })

    it('TC-204-3', (done) => {
      chai
        .request(server)
        .put('/api/studenthome/10000')
        .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
        .send({
          name: 'marco',
          address: 'leuvenaarvvstraat2',
          housenr: '52vvB',
          postalcode: '4463VK',
          telephone: '064099TEST',
          city: 'Breda'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          const response = res.body
          response.should.have.property('error').which.is.a('string')
          done()
        })
    })

    it('TC-204-4', (done) => {
      chai
        .request(server)
        .put('/api/studenthome/10000')
        .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
        .send({
          name: 'Name',
          address: 'Address',
          housenr: '1',
          postalcode: '4463EA',
          telephone: '0640883399',
          city: 'Goes'
        })
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')

          let { result } = res.body
          result.should.be.an('object')

          done()
        })
    })

    it('TC-204-5', (done) => {
      chai
        .request(server)
        .put('/api/studenthome/10000')
        .send({
          name: 'Name',
          address: 'NewAddress',
          housenr: '1B',
          postalcode: '4463EA',
          telephone: '0640883399',
          city: 'Breda'
        })
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')

          let { error } = res.body
          error.should.be.an('string')

          done()
        })
    })

    it('TC-204-6', (done) => {
      chai
        .request(server)
        .put('/api/studenthome/' + insertIdStudenthome)
        .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
        .send({
          name: 'testnameafterPut',
          address: 'streerfortestpurpose',
          housenr: '52',
          postalcode: '4461VA',
          telephone: '0640883399',
          city: 'testcity'
        })
        .end((err, res) => {
          let { result } = res.body
          result.should.be.an('object')

          done()
        })
    })
  }),
  describe('UC205 Delete studenthomes - DELETE /api/studenthome:homeId', () => {
    it('TC-205-1', (done) => {
      chai
        .request(server)
        .delete('/api/studenthome/1')
        .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')

          let { result } = res.body
          result.should.be.an('object')

          done()
        })
    })

    it('TC-205-2', (done) => {
      chai
        .request(server)
        .delete('/api/studenthome/91')
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')

          let { error } = res.body
          error.should.be.an('string')
          done()
        })
    })

    it('TC-205-3', (done) => {
      chai
        .request(server)
        .delete('/api/studenthome/91')
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')

          let { error } = res.body
          error.should.be.an('string')
          done()
        })
    })

    before((done) => {
      pool.query(INSERT_STUDENTHOME, [insertId], (err, rows, fields) => {
        if (err) {
          console.log(`beforeEach CLEAR_STUDENTHOME_TABLE: ${err}`)
          done(err)
        } else {
          insertIdStudenthome = rows.insertId
          done()
        }
      })
    })

    it('TC-205-4', (done) => {
      chai
        .request(server)
        .delete('/api/studenthome/' + insertIdStudenthome)
        .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          done()
        })
    })
  })

// ---------------------------------------------------------------------------------------------
// Optional testing for UC-206
// ---------------------------------------------------------------------------------------------

describe('UC206 Add students to studenthomes - PUIT /api/studenthome:homeId/user', () => {
  it('TC-206-1 Should return code 400 when user is not logged in', (done) => {
    chai
      .request(server)
      .put('/api/studenthome/' + insertIdStudenthome + '/user')
      .end((err, res) => {
        res.should.have.status(401)
        res.should.be.an('object')

        let { error } = res.body
        error.should.be.an('string')

        done()
      })
  })

  before((done) => {
    pool.query(INSERT_STUDENTHOME, [insertId], (err, rows, fields) => {
      if (err) {
        console.log(`beforeEach INSERT_STUDENTHOME: ${err}`)
        done(err)
      } else {
        insertIdStudenthome = rows.insertId
        done()
      }
    })
  })

  it('TC-206-2 Should return code 400 when user is not added', (done) => {
    chai
      .request(server)
      .put('/api/studenthome/' + insertIdStudenthome + '/user')
      .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
      .end((err, res) => {
        res.should.have.status(400)
        res.should.be.an('object')

        let { error } = res.body
        error.should.be.an('string')

        done()
      })
  })

  it('TC-206-3 Should return code 400 when user is already added to the studenthome', (done) => {
    chai
      .request(server)
      .put('/api/studenthome/' + insertIdStudenthome + '/user')
      .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
      .end((err, res) => {
        res.should.have.status(400)
        res.should.be.an('object')

        let { error } = res.body
        error.should.be.an('string')

        done()
      })
  })

  it('TC-206-4 Should return code 400 when studenthome does not exist', (done) => {
    chai
      .request(server)
      .put('/api/studenthome/1000000000000/user')
      .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
      .end((err, res) => {
        res.should.have.status(400)
        res.should.be.an('object')

        let { error } = res.body
        error.should.be.an('string')

        done()
      })
  })
})
