process.env.DB_DATABASE = process.env.DB_DATABASE || '2149386'
process.env.NODE_ENV = 'testing'
console.log(`Running tests using database '${process.env.DB_DATABASE}'`)

const pool = require('../../src/config/database')
const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../server')
const jwt = require('jsonwebtoken')

chai.should()
chai.use(chaiHttp)

var date = new Date()

const CLEAR_STUDENTHOME_TABLE = 'DELETE IGNORE FROM `studenthome`'
const CLEAR_USER_TABLE = 'DELETE IGNORE FROM `user`'
const CLEAR_MEAL_TABLE = 'DELETE IGNORE FROM `meal`'
const CLEAR_PARTICIPANTS_TABLE = 'DELETE IGNORE FROM `participants`'

const INSERT_USER =
  'INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES' +
  '("first", "last", "nameee@server.nl","1234567", "secret");'
let insertId

const INSERT_STUDENTHOME =
  'INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`,`Postal_Code`, `Telephone`, `City`) VALUES ("NameUser", "Address", "1", ? ,"4463EA", "0640883399", "Goes")'
let insertIdStudenthome

const INSERT_MEAL =
  'INSERT INTO `meal` (`Name`, `Description`, `Ingredients`,`Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudenthomeID`, `MaxParticipants`) VALUES ("NameUser", "Description", "Ingredients" ,"Allergies", "2000-09-09", "2000-09-09", "2", ?, ?, "10")'
let insertIdMeal

const INSERT_PARTICIPANT =
  'INSERT INTO `participants` (`UserID`, `StudenthomeID`, `MealID`, `SignedUpOn`) VALUES (?, ?, ?, "2000-09-09")'

before((done) => {
  pool.query(INSERT_USER, (err, rows, fields) => {
    if (err) {
      console.log(`before INSERT_USER: ${err}`)
      done(err)
    } else {
      insertId = rows.insertId
      done()
    }
  })
})

before((done) => {
  pool.query(CLEAR_STUDENTHOME_TABLE, (err, rows, fields) => {
    if (err) {
      console.log(`beforeEach CLEAR_STUDENTHOME_TABLE: ${err}`)
      done(err)
    } else {
      done()
    }
  })
})

before((done) => {
  pool.query(INSERT_STUDENTHOME, [insertId], (err, rows, fields) => {
    if (err) {
      console.log(`beforeEach CLEAR_STUDENTHOME_TABLE: ${err}`)
      done(err)
    } else {
      insertIdStudenthome = rows.insertId
      done()
    }
  })
})

before((done) => {
  pool.query(
    INSERT_MEAL,
    [insertId, insertIdStudenthome],
    (err, rows, fields) => {
      if (err) {
        console.log(`beforeEach CLEAR_STUDENTHOME_TABLE: ${err}`)
        done(err)
      } else {
        insertIdMeal = rows.insertId
        done()
      }
    }
  )
})

after((done) => {
  pool.query(CLEAR_PARTICIPANTS_TABLE, (err, rows, fields) => {
    if (err) {
      console.log(`after error: ${err}`)
      done(err)
    } else {
      done()
    }
  })
})

after((done) => {
  pool.query(CLEAR_MEAL_TABLE, (err, rows, fields) => {
    if (err) {
      console.log(`after error: ${err}`)
      done(err)
    } else {
      done()
    }
  })
})

after((done) => {
  pool.query(CLEAR_STUDENTHOME_TABLE, (err, rows, fields) => {
    if (err) {
      console.log(`after error: ${err}`)
      done(err)
    } else {
      done()
    }
  })
})

after((done) => {
  pool.query(CLEAR_USER_TABLE, (err, rows, fields) => {
    if (err) {
      console.log(`after error: ${err}`)
      done(err)
    } else {
      done()
    }
  })
})

describe('Manage participation', () => {
  describe('UC401 Manage registration - POST /api/studenthome/:id/meal/:id/signup', () => {
    it('TC-401-1', (done) => {
      chai
        .request(server)
        .post(
          '/api/studenthome/' +
            insertIdStudenthome +
            '/meal/' +
            insertIdMeal +
            '/signup'
        )
        .send({
          userid: insertId
        })
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')

          let { error } = res.body
          error.should.be.an('string')
          done()
        })
    })

    it('TC-401-3', (done) => {
      chai
        .request(server)
        .post(
          '/api/studenthome/' +
            insertIdStudenthome +
            '/meal/' +
            insertIdMeal +
            '/signup'
        )
        .set('authorization', 'Bearer ' + jwt.sign({ id: insertId }, 'secret'))
        .send({
          userid: insertId
        })
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result, UserID, StudenthomeID, MealID } = res.body
          result.should.be.an('string')

          done()
        })
    })
  }),
    describe('UC402 Manage cancellation - PUT /api/studenthome/:id/meal/:id/signoff', () => {
      it('TC-402-1', (done) => {
        chai
          .request(server)
          .put(
            '/api/studenthome/' +
              insertIdStudenthome +
              '/meal/' +
              insertIdMeal +
              '/signoff'
          )
          .send({
            userid: insertId
          })
          .end((err, res) => {
            res.should.have.status(401)
            res.should.be.an('object')

            let { error } = res.body
            error.should.be.an('string')
            done()
          })
      })

      it('TC-402-2', (done) => {
        chai
          .request(server)
          .put(
            '/api/studenthome/' + insertIdStudenthome + '/meal/1000000/signoff'
          )
          .set(
            'authorization',
            'Bearer ' + jwt.sign({ id: insertId }, 'secret')
          )
          .send({
            userid: insertId
          })
          .end((err, res) => {
            res.should.have.status(400)
            res.should.be.an('object')

            let { result } = res.body
            result.should.be.an('object')
            done()
          })
      })

      it('TC-402-3', (done) => {
        chai
          .request(server)
          .put(
            '/api/studenthome/' +
              insertIdStudenthome +
              '/meal/' +
              insertIdMeal +
              '/signoff'
          )
          .set(
            'authorization',
            'Bearer ' + jwt.sign({ id: insertId }, 'secret')
          )
          .send({
            userid: '1010101010101'
          })
          .end((err, res) => {
            res.should.have.status(200)
            res.should.be.an('object')

            let { result } = res.body
            result.should.be.an('string')
            done()
          })
      })

      it('TC-402-4', (done) => {
        chai
          .request(server)
          .put(
            '/api/studenthome/' +
              insertIdStudenthome +
              '/meal/' +
              insertIdMeal +
              '/signoff'
          )
          .set(
            'authorization',
            'Bearer ' + jwt.sign({ id: insertId }, 'secret')
          )
          .send({
            userid: insertId
          })
          .end((err, res) => {
            res.should.have.status(400)
            res.should.be.an('object')

            done()
          })
      })
    }),
    describe('UC403 Manage lists - GET /api/meal/:id/participants', () => {
      it('TC-403-1', (done) => {
        chai
          .request(server)
          .get('/api/meal/' + insertIdMeal + '/participants')
          .send()
          .end((err, res) => {
            res.should.have.status(401)
            res.should.be.an('object')

            done()
          })
      })

      it('TC-403-2', (done) => {
        chai
          .request(server)
          .get('/api/meal/10000000/participants')
          .set(
            'authorization',
            'Bearer ' + jwt.sign({ id: insertId }, 'secret')
          )
          .send()
          .end((err, res) => {
            res.should.have.status(400)
            res.should.be.an('object')

            done()
          })
      })

      before((done) => {
        pool.query(
          INSERT_PARTICIPANT,
          [insertId, insertIdStudenthome, insertIdMeal],
          (err, rows, fields) => {
            if (err) {
              console.log(`beforeEach INSERT PARTICIPANT: ${err}`)
              done(err)
            } else {
              done()
            }
          }
        )
      })

      it('TC-403-3', (done) => {
        chai
          .request(server)
          .get('/api/meal/' + insertIdMeal + '/participants')
          .set(
            'authorization',
            'Bearer ' + jwt.sign({ id: insertId }, 'secret')
          )
          .send()
          .end((err, res) => {
            res.should.have.status(200)
            res.should.be.an('object')

            done()
          })
      })
    }),
    describe('UC404 Manage specific lists - GET /api/meal/:id/participants/:id', () => {
      it('TC-404-1', (done) => {
        chai
          .request(server)
          .get('/api/meal/' + insertIdMeal + '/participants/' + insertId)
          .send()
          .end((err, res) => {
            res.should.have.status(401)
            res.should.be.an('object')

            let { error } = res.body
            error.should.be.an('string')
            done()
          })
      })

      it('TC-404-2', (done) => {
        chai
          .request(server)
          .get('/api/meal/' + insertIdMeal + '/participants/10010101010')
          .send()
          .end((err, res) => {
            res.should.have.status(401)
            res.should.be.an('object')

            let { error } = res.body
            error.should.be.an('string')
            done()
          })
      })

      it('TC-404-3', (done) => {
        chai
          .request(server)
          .get('/api/meal/' + insertIdMeal + '/participants/' + insertId)
          .set(
            'authorization',
            'Bearer ' + jwt.sign({ id: insertId }, 'secret')
          )
          .send()
          .end((err, res) => {
            res.should.have.status(200)
            res.should.be.an('object')

            const response = res.body

            response.should.have.property('ID').which.is.a('number')
            response.should.have.property('Name').which.is.a('string')
            response.should.have.property('Email').which.is.a('string')
            response.should.have.property('Studentnumber').which.is.a('string')

            done()
          })
      })
    })
})
